import { gql } from '@apollo/client';

const GET_USER = gql`
  query GetUser($id: ID!) {
    getUser(_id: $id) {
      _id
      firstName
      lastName
      age
      phone
      email
      register
      role
    }
  }
`;
const GET_USERS = gql`
  query GetUsers {
    getUsers {
      _id
      firstName
      lastName
      age
      phone
      email
      register
      role
    }
  }
`;

const CREATE_USER = gql`
  mutation CreateUser(
    $firstName: String
    $lastName: String
    $age: String
    $phone: String
    $email: String!
    $register: String
    $password: String!
    $role: String
  ) {
    createUser(
      firstName: $firstName
      lastName: $lastName
      age: $age
      phone: $phone
      email: $email
      register: $register
      password: $password
      role: $role
    ) {
      _id
      firstName
      lastName
      age
      phone
      email
      register
      password
      role
    }
  }
`;

const UPDATE_USER = gql`
  mutation UpdateUser(
    $id: ID!
    $firstName: String
    $lastName: String
    $age: String
    $phone: String
    $email: String
    $register: String
    $password: String
    $role: String
  ) {
    updateUser(
      _id: $id
      firstName: $firstName
      lastName: $lastName
      age: $age
      phone: $phone
      email: $email
      password: $password
      register: $register
      role: $role
    ) {
      _id
      firstName
      lastName
      age
      phone
      email
      register
      password
      role
    }
  }
`;

const DELETE_USER = gql`
  mutation DeleteUser($id: ID!) {
    deleteUser(_id: $id)
  }
`;

const LOGIN_USER = gql`
  mutation Login($email: String!, $password: String!) {
    login(email: $email, password: $password)
  }
`;

const GET_CURRENT_USER = gql`
  query GetCurrentUser {
    getCurrentUser {
      isAuth
      userId
      userRole
    }
  }
`;

export {
  GET_USER,
  GET_USERS,
  CREATE_USER,
  DELETE_USER,
  UPDATE_USER,
  LOGIN_USER,
  GET_CURRENT_USER,
};
