import { ApolloClient, InMemoryCache, createHttpLink } from '@apollo/client';
import { API } from '../../utils/constants/constants';
import { setContext } from '@apollo/client/link/context';

const httpLink = createHttpLink({
  uri: `${API}/graphql`,
  credentials: 'same-origin',
});

const authLink = setContext((_, { headers }) => {
  const token = localStorage.getItem('token');
  return {
    headers: {
      ...headers,
      authorization: token ? `${token}` : '',
    },
  };
});

const client = new ApolloClient({
  link: authLink.concat(httpLink),
  cache: new InMemoryCache(),
});

export default client;
