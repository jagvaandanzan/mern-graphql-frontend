import { useState } from 'react';
import Main from './pages/Main/Main';
import Login from './pages/Login/Login';
import Navbar from './component/Navbar';
import { Routes, Route, Navigate } from 'react-router-dom';
import Register from './pages/Register/Register';
import { GET_CURRENT_USER } from './graphql/gql/user/user';
import { useQuery } from '@apollo/client';

function App() {
  const [showAddUser, setShowAddUser] = useState(false);
  const { data, error, loading } = useQuery(GET_CURRENT_USER);
  console.log(data?.getCurrentUser);
  if (error) return <div>{error.message}</div>;
  return (
    <>
      <Navbar />
      <Routes>
        {data?.getCurrentUser.isAuth ? (
          <>
            <Route index element={<Main />} />
            <Route path="*" element={<Navigate to="/" />} />
          </>
        ) : (
          <>
            <Route path="/login" element={<Login />} />
            <Route path="/register" element={<Register />} />
            <Route path="*" element={<Navigate to="/login" />} />
          </>
        )}
      </Routes>
    </>
  );
}

export default App;
