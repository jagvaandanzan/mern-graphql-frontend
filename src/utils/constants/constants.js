const PORT = 8080;

const API = `http://localhost:${PORT}`;

const ROLES = {
  USER: 'USER',
  ADMIN: 'ADMIN',
};

export { API, ROLES };
