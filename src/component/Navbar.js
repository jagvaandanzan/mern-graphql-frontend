export default function Navbar() {
  return (
    <div>
      <nav className="flex justify-center items-center h-24 bg-sky-700">
        <p className="text-5xl font-bold text-white">Хэрэглэгчийн бүртгэл</p>
      </nav>
    </div>
  );
}
