import React, { useState } from 'react';
import { useMutation, useQuery } from '@apollo/client';
import toast, { Toaster } from 'react-hot-toast';
import { GET_USER, UPDATE_USER } from '../graphql/gql/user/user';
import { ROLES } from '../utils/constants/constants';
import {
  Snackbar,
  Alert,
  CircularProgress,
  Grid,
  FormControlLabel,
  Switch,
} from '@mui/material';

export default function EditUser({ selectedId }) {
  const [firstName, setFirstName] = useState();
  const [lastName, setLastName] = useState('');
  const [age, setAge] = useState('');
  const [mail, setMail] = useState('');
  const [register, setRegister] = useState('');
  const [phone, setPhone] = useState('');
  const [password, setPassword] = useState('');
  const [admin, setAdmin] = useState(false);

  const { loading, error, data } = useQuery(GET_USER, {
    variables: {
      id: selectedId,
    },
    onCompleted(data) {
      setFirstName(data.getUser.firstName);
      setLastName(data.getUser.lastName);
      setAge(data.getUser.age);
      setMail(data.getUser.email);
      setRegister(data.getUser.register);
      setPhone(data.getUser.phone);
      setAdmin(data.getUser.role === 'ADMIN' ? true : false);
    },
  });

  const [updateUser] = useMutation(UPDATE_USER);

  return (
    <div className="w-screen h-screen font-sans">
      <main className="flex mt-16 justify-center">
        <form className="flex flex-col space-y-5">
          <input
            className="h-8 border-2 border-sky-700 rounded-xl pl-3"
            type="text"
            placeholder="Овог"
            name="lastName"
            value={lastName}
            onChange={(e) => {
              setLastName(e.target.value);
            }}
          />
          <input
            className="h-8 border-2 border-sky-700 rounded-xl pl-3"
            type="text"
            placeholder="Нэр"
            name="firstName"
            value={firstName}
            onChange={(e) => {
              setFirstName(e.target.value);
            }}
          />
          <input
            className="h-8 border-2 border-sky-700 rounded-xl pl-3"
            type="number"
            placeholder="Нас"
            name="age"
            value={age}
            onChange={(e) => {
              setAge(e.target.value);
            }}
          />
          <input
            className="h-8 border-2 border-sky-700 rounded-xl pl-3"
            type="text"
            placeholder="Регистр"
            name="register"
            value={register}
            onChange={(e) => {
              setRegister(e.target.value);
            }}
          />
          <input
            className="h-8 border-2 border-sky-700 rounded-xl pl-3"
            type="text"
            placeholder="мейл"
            name="email"
            value={mail}
            onChange={(e) => {
              setMail(e.target.value);
            }}
          />
          <input
            className="h-8 border-2 border-sky-700 rounded-xl pl-3"
            type="number"
            placeholder="Утас"
            name="phone"
            value={phone}
            onChange={(e) => {
              setPhone(e.target.value);
            }}
          />
          <input
            className="h-8 border-2 border-sky-700 rounded-xl pl-3"
            type="password"
            placeholder="Нууц үг"
            value={password}
            onChange={(e) => {
              setPassword(e.target.value);
            }}
          />
          <FormControlLabel
            control={
              <Switch checked={admin} onChange={() => setAdmin(!admin)} />
            }
            label="Admin"
          />
          <button
            className="bg-sky-700 text-white hover:bg-sky-800 rounded-xl h-8"
            onClick={() => {
              updateUser({
                variables: {
                  id: selectedId,
                  firstName: firstName,
                  lastName: lastName,
                  age: age,
                  phone: phone,
                  email: mail,
                  register: register,
                  password: password,
                  role: admin ? ROLES.ADMIN : ROLES.USER,
                },
                onCompleted() {
                  toast.success('Амжилттай заслаа');
                },
                onError(e) {
                  console.log(e.message);
                },
              });
            }}
          >
            Засах
          </button>
          <Toaster position="top-center" reverseOrder={false} />
        </form>
      </main>
    </div>
  );
}
