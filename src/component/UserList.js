import { useQuery, useMutation } from '@apollo/client';
import { useState } from 'react';
import {
  DELETE_USER,
  GET_USERS,
  GET_CURRENT_USER,
  GET_USER,
} from '../graphql/gql/user/user';

import EditUser from './EditUser';

export default function UserList() {
  const [selected, setSelected] = useState(false);
  const [selectedId, setSelectedId] = useState();
  const [deleteUser] = useMutation(DELETE_USER);
  const { data } = useQuery(GET_CURRENT_USER);
  console.log(data);
  const { data: usersData, loading, error } = useQuery(GET_USERS);
  const { data: currentUser } = useQuery(GET_USER, {
    variables: {
      id: data?.getCurrentUser.userId,
    },
  });
  if (loading) return <div className="flex justify-center mt-20">loading</div>;
  if (error)
    return <div className="flex justify-center mt-20">{error.message}</div>;

  if (selected) return <EditUser selectedId={selectedId} />;

  return (
    <div className="w-full flex justify-center mt-8">
      <ul className="space-y-4">
        {usersData.getUsers.map((user) => (
          <li key={user._id} className="flex items-center">
            <p
              className="border border-sky-700 rounded-xl p-2 grow hover:cursor-pointer"
              onClick={() => {
                setSelectedId(user._id);
                if (currentUser?.getUser.role === 'ADMIN')
                  setSelected(!selected);
              }}
            >
              {user.lastName}-{user.firstName}
            </p>
            {currentUser?.getUser.role === 'ADMIN' ? (
              <span
                className="hover:cursor-pointer"
                onClick={() => {
                  deleteUser({
                    variables: {
                      id: user._id,
                    },
                    update(cache, { data }) {
                      const existingUsers = cache.readQuery({
                        query: GET_USERS,
                      });
                      const newUsers = existingUsers.getUsers.filter(
                        (t) => t._id !== user._id
                      );
                      cache.writeQuery({
                        query: GET_USERS,
                        data: { getUsers: newUsers },
                      });
                    },
                  });
                }}
              >
                &#10006;
              </span>
            ) : (
              ''
            )}
          </li>
        ))}
      </ul>
    </div>
  );
}
