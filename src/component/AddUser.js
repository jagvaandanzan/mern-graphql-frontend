import React, { useState } from 'react';
import toast, { Toaster } from 'react-hot-toast';
import { useMutation } from '@apollo/client';
import { CREATE_USER } from '../graphql/gql/user/user';
import { useNavigate } from 'react-router-dom';
import {
  Snackbar,
  Alert,
  CircularProgress,
  Grid,
  FormControlLabel,
  Switch,
} from '@mui/material';

import { ROLES } from '../utils/constants/constants';

export default function AddUser() {
  const [firstName, setFirstName] = useState();
  const [lastName, setLastName] = useState('');
  const [age, setAge] = useState('');
  const [mail, setMail] = useState('');
  const [register, setRegister] = useState('');
  const [phone, setPhone] = useState('');
  const [password, setPassword] = useState('');
  const [open, setOpen] = useState(false);
  const [admin, setAdmin] = useState(false);
  const navigate = useNavigate();
  const [addUser, { data, loading, error }] = useMutation(CREATE_USER, {
    variables: {
      firstName: firstName,
      lastName: lastName,
      age: age,
      phone: phone,
      email: mail,
      register: register,
      password: password,
    },
    notifyOnNetworkStatusChange: true,
    onCompleted(data) {
      toast.success('Амжилттай нэмлээ');
      (() => setOpen(true))();
      setFirstName('');
      setLastName('');
      setAge('');
      setMail('');
      setRegister('');
      setPhone('');
      setPassword('');
      setTimeout(() => {
        console.log('settimeout');
        navigate('/login', { replace: true });
      }, 5000);
    },
  });

  if (loading)
    return (
      <Grid
        container
        direction="row"
        justifyContent="center"
        alignItems="center"
        marginTop={8}
      >
        <CircularProgress />
      </Grid>
    );
  if (error) {
    console.log(error.message);
    return (
      <div className="justify-center mt-20">
        <div className="grid justify-items-center space-y-5">
          <p>Алдаа гарлаа дахин оролдоно уу</p>
          <button
            className="bg-sky-700 text-white hover:bg-sky-800 rounded-xl w-12 h-8"
            onClick={() => window.location.reload(false)}
          >
            Эхлэх
          </button>
        </div>
      </div>
    );
  }
  return (
    <div className="w-screen h-screen font-sans">
      <Snackbar
        open={open}
        autoHideDuration={6000}
        onClose={(e, r) => r === 'clickaway' && setOpen(false)}
      >
        <Alert
          onClose={() => setOpen(false)}
          severity="success"
          sx={{ width: '100%' }}
        >
          Амжилттай бүртгэлээ
        </Alert>
      </Snackbar>
      <main className="flex mt-16 justify-center">
        <form className="flex flex-col space-y-5">
          <input
            className="h-8 border-2 border-sky-700 rounded-xl pl-3"
            type="text"
            placeholder="Регистр"
            name="register"
            value={register}
            onChange={(e) => {
              setRegister(e.target.value);
            }}
          />
          <input
            className="h-8 border-2 border-sky-700 rounded-xl pl-3"
            type="text"
            placeholder="Овог"
            name="lastName"
            value={lastName}
            onChange={(e) => {
              setLastName(e.target.value);
            }}
          />
          <input
            className="h-8 border-2 border-sky-700 rounded-xl pl-3"
            type="text"
            placeholder="Нэр"
            name="firstName"
            value={firstName}
            onChange={(e) => {
              setFirstName(e.target.value);
            }}
          />
          <input
            className="h-8 border-2 border-sky-700 rounded-xl pl-3"
            type="number"
            placeholder="Нас"
            name="age"
            value={age}
            onChange={(e) => {
              setAge(e.target.value);
            }}
          />
          <input
            className="h-8 border-2 border-sky-700 rounded-xl pl-3"
            type="number"
            placeholder="Утас"
            name="phone"
            value={phone}
            onChange={(e) => {
              setPhone(e.target.value);
            }}
          />
          <input
            className="h-8 border-2 border-sky-700 rounded-xl pl-3"
            type="text"
            placeholder="мейл"
            name="email"
            value={mail}
            onChange={(e) => {
              setMail(e.target.value);
            }}
          />
          <input
            className="h-8 border-2 border-sky-700 rounded-xl pl-3"
            type="password"
            placeholder="Нууц үг"
            value={password}
            onChange={(e) => {
              setPassword(e.target.value);
            }}
          />
          <button
            className="bg-sky-700 text-white hover:bg-sky-800 rounded-xl h-8"
            onClick={addUser}
          >
            Бүртгүүлэх
          </button>
          <Toaster position="top-center" reverseOrder={false} />
        </form>
      </main>
    </div>
  );
}
