import AddUser from '../../component/AddUser';
import { useState } from 'react';
import UserList from '../../component/UserList';
import { useNavigate } from 'react-router-dom';
import { useApolloClient } from '@apollo/client';

export default function Main() {
  const navigate = useNavigate();
  const client = useApolloClient();
  return (
    <>
      <div className="flex justify-center mt-5">
        <button
          className="bg-sky-700 text-white hover:bg-sky-800 rounded-xl h-8 w-32"
          onClick={() => {
            localStorage.removeItem('token');
            client.resetStore();
            // window.location.reload(false);
            navigate('/login', { replace: true });
          }}
        >
          Гарах
        </button>
      </div>
      <UserList />
    </>
  );
}
