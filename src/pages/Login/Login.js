import {
  Box,
  Button,
  Container,
  TextField,
  Typography,
  Snackbar,
  Alert,
} from '@mui/material';
import { useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import { useMutation, useApolloClient } from '@apollo/client';
import { LOGIN_USER } from '../../graphql/gql/user/user';

export default function Login() {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [open, setOpen] = useState(false);
  const [errorMessage, setErrorMessage] = useState();
  const navigate = useNavigate();
  const client = useApolloClient();
  const [login, { data, error, loading }] = useMutation(LOGIN_USER, {
    onCompleted(data) {
      localStorage.setItem('token', data.login);
      console.log(data.login + '123');
      client.reFetchObservableQueries();
      navigate('/', { replace: true });
    },
    onError(err) {
      setErrorMessage(err.message);
    },
  });

  function checkFields() {
    console.log(133);
    if (email === '' || password === '') {
      setErrorMessage('Та бүрэн мэдээлэл оруулна уу');
      return false;
    } else {
      return true;
    }
  }

  return (
    <div>
      <Snackbar
        open={open}
        autoHideDuration={6000}
        onClose={(e, r) => r === 'clickaway' && setOpen(false)}
      >
        <Alert
          onClose={() => setOpen(false)}
          severity="error"
          sx={{ width: '100%' }}
        >
          {errorMessage}
        </Alert>
      </Snackbar>
      <Container maxWidth="xs" sx={{ mt: 2 }}>
        <Box sx={{ pt: 2 }}>
          <TextField
            fullWidth
            label="И-Мейл"
            variant="outlined"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
        </Box>
        <Box sx={{ pt: 2 }}>
          <TextField
            fullWidth
            type="password"
            label="Нууц үг"
            variant="outlined"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          />
        </Box>

        <Button
          fullWidth
          variant="contained"
          sx={{ mt: 3 }}
          onClick={() => {
            if (checkFields()) {
              login({
                variables: {
                  email: email,
                  password: password,
                },
              });
            }
            setOpen(true);
          }}
        >
          Нэвтрэх
        </Button>

        <Typography
          align="center"
          variant="body1"
          sx={{ mt: 3, color: '#1976d2' }}
        >
          <Link to="/register">Бүртгэл үүсгэх</Link>
        </Typography>
      </Container>
    </div>
  );
}
